import { LexerError } from './Lexer';

class UnknowType extends LexerError {
    constructor(message: string) {
        super(`did not find any rule that match with : ${message}`);
    }
}

export { UnknowType as LexerUnknowType };
