import { LexerError } from './Lexer';

test('basic', () => {
    expect(new LexerError('bonjour'))
        .toEqual(new Error('Lexer: bonjour'));
});
test('no message', () => {
    expect(new LexerError())
        .toEqual(new Error('Lexer: unknow error'));
});
