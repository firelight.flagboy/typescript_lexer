import { LexerError } from './Lexer';
import { LexerUnknowType } from './UnknowType';

test('basic', () => {
    expect(new LexerUnknowType('bonjour'))
        .toEqual(new LexerError('did not find any rule that match with : bonjour'));
});
