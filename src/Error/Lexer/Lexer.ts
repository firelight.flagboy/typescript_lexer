
class LexerError extends Error {
    private static readonly LEXER_PREFIX = 'Lexer: ';
    private static readonly LEXER_UNKNOW = 'unknow error';

    constructor(message?: string) {
        if (message)
            super(LexerError.LEXER_PREFIX + message);
        else
            super(LexerError.LEXER_PREFIX + LexerError.LEXER_UNKNOW);
    }
}

export { LexerError };
