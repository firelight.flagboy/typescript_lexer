import { LexerDuplicatedTypeError } from './DuplicatedType';
import { LexerError } from './Lexer';

test('basic', () => {
    expect(new LexerDuplicatedTypeError('string'))
        .toEqual(new LexerError('rules: found duplicated type: string'));
});

test('basic with number for id', () => {
    expect(new LexerDuplicatedTypeError(42))
        .toEqual(new LexerError('rules: found duplicated type: 42'));
});

test('string id with index', () => {
    expect(new LexerDuplicatedTypeError('string', 42))
        .toEqual(new LexerError('rules: found duplicated type: string at 42'));
});

test('number id with index', () => {
    expect(new LexerDuplicatedTypeError(42, 21))
        .toEqual(new LexerError('rules: found duplicated type: 42 at 21'));
});
