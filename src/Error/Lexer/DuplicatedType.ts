import { LexerError } from './Lexer';

class DuplicatedType extends LexerError {
    constructor(type: TID, index?: number) {
        if (index !== undefined)
            super(`rules: found duplicated type: ${type} at ${index}`);
        else
            super(`rules: found duplicated type: ${type}`);
    }
}

export { DuplicatedType as LexerDuplicatedTypeError };
