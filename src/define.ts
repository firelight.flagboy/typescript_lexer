type TID = string | number;

interface IRule {
    type: TID;
    check: RegExp;
}

type TLexerRules = IRule[];
