import { Token } from './Token';

test('basic', () => {
    expect(new Token('bonjour', 'test').toString())
    .toEqual('Token(bonjour, test)');
});

test('basic num as ID', () => {
    expect(new Token(42, 'test').toString())
    .toEqual('Token(42, test)');
});
