class Token {
    constructor(private readonly type: TID, private readonly lexem: string) {}

    public toString(): string {
        const { type, lexem } = this;

        return `Token(${type}, ${lexem})`;
    }
}

export { Token };
