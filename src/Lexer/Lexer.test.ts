import { LexerDuplicatedTypeError } from '../Error/Lexer/DuplicatedType';
import { LexerError } from '../Error/Lexer/Lexer';
import { LexerUnknowType } from '../Error/Lexer/UnknowType';
import { Lexer } from './Lexer';

test('basic', () => {
    const rules: TLexerRules = [
        {
            check: /test/,
            type: 'hello',
        },
        {
            check: /bonjour/,
            type: 'test',
        },
    ];

    expect(new Lexer(rules)).not
        .toBeUndefined();
});

test('duplicated type string', () => {
    const rules: TLexerRules = [
        {
            check: /test/,
            type: 'string',
        },
        {
            check: /bonjour/,
            type: 'string',
        },
    ];

    expect(() => new Lexer(rules))
        .toThrow(new LexerDuplicatedTypeError('string', 1));
});

test('duplicated type number', () => {
    const rules: TLexerRules = [
        {
            check: /test/,
            type: 0,
        },
        {
            check: /bonjour/,
            type: 0,
        },
    ];

    expect(() => new Lexer(rules))
        .toThrowError(new LexerDuplicatedTypeError(0, 1));
});

test('duplicated type number and string', () => {
    const rules: TLexerRules = [
        {
            check: /test/,
            type: 0,
        },
        {
            check: /bonjour/,
            type: '0',
        },
    ];

    expect(new Lexer(rules)).not
        .toBeUndefined();
});

test('check value when no input set', () => {
    const lexer = new Lexer([]);

    expect(lexer.getInput())
        .toBeUndefined();
});

test('set input in constructor', () => {
    const rules: TLexerRules = [];
    const message = 'bonjour je suis la';
    const lexer = new Lexer(rules, message);

    expect(lexer.getInput())
        .toEqual(message);
});

test('set input in after constructor', () => {
    const message = 'bonjour je suis la';
    const lexer = new Lexer([]);

    lexer.setInput(message);
    expect(lexer.getInput())
        .toEqual(message);
});

test('basic test Lexer::lexe method', () => {
    const rules: TLexerRules = [
        {
            check: /^\d+/,
            type: 'number',
        },
        {
            check: /^\w+/,
            type: 'word',
        },
        {
            check: /^\s+/,
            type: 'space',
        },
    ];
    const lexer = new Lexer(rules);

    lexer.setInput('bonjour je suis la');
    expect(lexer.lexe())
        .toEqual([
            { lexem: 'bonjour', type: 'word' },
            { lexem: ' ', type: 'space' },
            { lexem: 'je', type: 'word' },
            { lexem: ' ', type: 'space' },
            { lexem: 'suis', type: 'word' },
            { lexem: ' ', type: 'space' },
            { lexem: 'la', type: 'word' },
        ]);
});

test('other test for Lexer::lexe', () => {
    const rules: TLexerRules = [
        {
            check: /^\d+/,
            type: 'number',
        },
        {
            check: /^\w+/,
            type: 'word',
        },
        {
            check: /^\s+/,
            type: 'space',
        },
    ];
    const lexer = new Lexer(rules);

    lexer.setInput('bonjour je reviens dans     42 semaine\ts');
    expect(lexer.lexe())
        .toEqual([
            { lexem: 'bonjour', type: 'word' },
            { lexem: ' ', type: 'space' },
            { lexem: 'je', type: 'word' },
            { lexem: ' ', type: 'space' },
            { lexem: 'reviens', type: 'word' },
            { lexem: ' ', type: 'space' },
            { lexem: 'dans', type: 'word' },
            { lexem: '     ', type: 'space' },
            { lexem: '42', type: 'number' },
            { lexem: ' ', type: 'space' },
            { lexem: 'semaine', type: 'word' },
            { lexem: '\t', type: 'space' },
            { lexem: 's', type: 'word' },
        ]);
});

test('Lexer::lexe unknow type', () => {
    const rules: TLexerRules = [
        {
            check: /^[a-z]+/,
            type: 'word',
        },
        {
            check: /^\s+/,
            type: 'space',
        },
    ];
    const lexer = new Lexer(rules);

    lexer.setInput('bonjour je reviens dans     42 semaine\ts');
    expect(() => lexer.lexe())
        .toThrow(new LexerUnknowType('42 semaine\ts'));
}, 1000);

test('undefined input string Lexer::lexer', () => {
    const lexer = new Lexer([]);

    expect(() => lexer.lexe())
        .toThrow(new LexerError('Input string is undefined'));
});
