import { LexerDuplicatedTypeError } from '../Error/Lexer/DuplicatedType';
import { LexerError } from '../Error/Lexer/Lexer';
import { LexerUnknowType } from '../Error/Lexer/UnknowType';
import { Token } from '../Token/Token';

class Lexer {

    public input: string | undefined;

    constructor(
        private readonly rules: TLexerRules,
        input?: string,
    ) {
        if (input)
            this.setInput(input);
        this.checkDuplicatedTypeInRules();
    }

    public setInput(input: string): void {
        this.input = input;
    }

    public getInput(): string | undefined {
        return this.input;
    }

    public checkDuplicatedTypeInRules(): boolean {
        const encounteredType: TID[] = [];

        this.rules.forEach((rule: IRule, index: number) => {
            if (encounteredType.indexOf(rule.type) !== -1)
                throw new LexerDuplicatedTypeError(rule.type, index);
            encounteredType.push(rule.type);
        });
        return true;
    }

    public lexe(): Token[] {
        const tokens: Token[] = [];
        let haveMatch = false;

        if (this.input === undefined)
            throw new LexerError('Input string is undefined');
        while (this.input.length > 0) {
            haveMatch = false;
            for (const rule of this.rules) {
                const match = rule.check.exec(this.input);
                if (match && match[0]) {
                    haveMatch = true;
                    this.input = this.input.slice(match[0].length);
                    tokens.push(new Token(rule.type, match[0]));
                    break;
                }
            }
            if (!haveMatch)
                throw new LexerUnknowType(this.input);
        }
        return tokens;
    }
}

export { Lexer };
